from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView
from books.models import Books
from actions import my_book

class Home(TemplateView):
    template_name = 'books/index.html'


class MYBooks(ListView):
    template_name = 'books/books.html'
    model = Books

    def get_context_data(self, **kwargs):
        context = super(MYBooks, self).get_context_data(**kwargs)
        context['book'] = my_book()
        return context


        
