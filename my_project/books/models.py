from django.db import models

class Books(models.Model):
	name = models.CharField(max_length=50)
	author = models.CharField(max_length=50)

	def __unicode__(self):
		return '{0} {1}'.format(self.name, self.author)